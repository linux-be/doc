#!/bin/bash

image_name="registry.gitlab.com/linux-be/doc"
container_name=docs

docker run --rm -it \
       -v "$(pwd):/src" \
       -u="$UID:$(id -g $USER)" \
       --name "$container_name" "$image_name" \
       sh -c "cd /src && make -j"
