\chapter{beadm} \label{chapter-beadm} % beadm
\markboth{}{beadm}

\texttt{beadm} is a command line interface for administering boot environments.

% TODO Show the use cases (with example output for ``list'')

\section{Porting from illumos}

\subsection{Rationale}

% TODO Link Solaris
In Solaris-based operating systems,
it uses the \texttt{libbe} library for the core logic.
Both the interface and the library are written in C.

In the previous attempts to bring \texttt{beadm} to GNU/Linux,
it was implemented as a shell script. In one, it was written
from scratch~\cite{attempt-1} and only supported the basic features. In
another, it was ported from FreeBSD~\cite{attempt-3} and had some
differences from the illumos behavior and some bugs not present in
illumos~\cite{attempt-1-beadm-features}.

Therefore, it was decided to port the \texttt{beadm} source code from
illumos. As a result, the core behavior is the same.

Since the source code is largely the same, it is possible to add an
improvement or fix an issue once on one platform and then port the
solution to the other. Several such issues have been
found~\cite{issues-with-upstream-tag} and one of them
fixed~\cite{fixed-upstream-issue}. They are going to be reported and
the solutions ported upstream.

\subsection{Approach}

The goal was to make the \texttt{beadm} and \texttt{libbe} C code work on
GNU/Linux with minimal changes. This would make porting changes to the
codebase between platforms straightforward.

\subsubsection{The choice of a build system}

\texttt{libbe} uses \texttt{libzfs} for operations on ZFS pools and
datasets. \texttt{libzfs} on GNU/Linux is provided by ZFSonLinux (ZoL)'s
\texttt{zfs} repository/package~\cite{zfsonlinux/zfs}.

The illumos codebase has a Makefile-based build system,
which does not work on GNU/Linux out of the box~\cite{how-to-build-illumos}.
ZFSonLinux already builds code derived from illumos on GNU/Linux.
It uses a build system based on GNU Autotools~\cite{autotools}. It was
decided to use an Autotools-based build system as well.

To speed up the creation of a working version, the existing setup from
\texttt{zfsonlinux/zfs} was reused -- the repository was forked and
the \texttt{libbe} and \texttt{beadm} code from illumos was added to
the fork.

As a result, much less setup was required than would be for a separate
repository.
\begin{itemize}
\item It is possible to build binary packages with \texttt{libbe} and
  \texttt{beadm} not only for Gentoo, but for all the distributions
  supported by ZFSonLinux.
\item Tests for \texttt{beadm} can be added to the ZFS test suite in
  the repository, and the convenience scripts for running parts of the
  suite can be used as is.
\end{itemize}

\subsubsection{Preserving upstream code's history}

Git subtrees were used to preserve the commit history of upstream code.

The procedure was as follows~\cite{import-repo-subdir-as-subtree,
  import-repo-subdir-as-subtree-to-linux-be}:
\begin{enumerate}
\item Add the upstream repository
  (\texttt{illumos-gate}~\cite{illumos-gate}) as a remote.
\item Add a copy of its master branch to the \texttt{zfs} repository.
\item Use \texttt{git filter-branch} to replace empty idents
  in commits with a default value.

  This is necessary because the upstream repository contains some
  commits with an empty ident (authorship information), which makes the
  \texttt{git subtree} commands used later below return an error.

\item Then, for each subdirectory that needs to be imported:
  \begin{enumerate}
  \item Split a subtree from a subdirectory in the branch.
  \item Add the subtree (with a merge commit) to a feature branch.
  \item Remove files unnecessary for GNU/Linux and/or \texttt{beadm}
    (with an additional commit).
  \item Adjust the paths of files to match the ZFSonLinux structure
    (with an additional commit).
  \item Add the path adjustments done to the
    \texttt{scripts/zfs2zol-patch.sed} script in the repository.

    The script is used in the ZFSonLinux project to adjust the paths
    in patches from upstream (OpenZFS) for applying them to the Linux
    version.
  \end{enumerate}
  % TODO Describe gitlab feature branch / MR workflow?
\end{enumerate}

This allows pulling in future upstream changes with a \texttt{git merge}
command. But, more importantly, as a result, it is possible to use
\texttt{git blame} and see when and by whom (and, sometimes, why; but, more
often, as part of which feature) any piece of code was introduced.

However, there were some downsides to this approach:

\begin{itemize}
\item While the commit messages contained IDs of the related tasks/issues,
  only the illumos bugtracker IDs were useful, as a backup of the
  OpenSolaris bugtracker could not be found.

\item It was not possible to use \texttt{git rebase} past the commit that
  added the subtree. The \texttt{--preserve-merges} option was not
  effective, as \texttt{git rebase} could not recreate merges made with
  \texttt{git subtree add}.

\item Using \texttt{git filter-branch} on the copy of the upstream master
  branch made it different from upstream, so the upstream changes would
  probably not be mergeable directly.

\item It was not possible to import a subset of a directory's files from
  upstream, so whole directories (e.g. \texttt{man.1m}) had to be imported.

  The resulting repository was considerably larger than the original
  \texttt{zfsonlinux/zfs} repository, as it contained the history of a few
  subdirectories from illumos since 2005 (with mostly files unnecessary for
  \texttt{beadm}). This meant it took longer to clone it.

  One solution tried was to remove the unnecessary files with
  \texttt{git filter-branch} prior to splitting the subtree, but the experiments
  performed extremely badly. Additionally, this would not be practical, as
  this would be an additional modification to the upstream branch before
  splitting subtrees from it.
\end{itemize}

To compare, the ZFSonLinux project followed a different approach --
squashing upstream (then OpenSolaris) code with the changes needed to
port it into a few large commits initially, and later porting changes
from upstream (OpenZFS) by using \texttt{git am} and squashing the
porting-specific changes to the resulting commits. As a result,
original authorship information is available for the later additions,
but absent for the earlier ones; and the porting-specific changes are
not separated from the illumos code (though some porters try to remedy
that with a ``Porting notes'' section in the commit messages).

\section{Differences from illumos}

Some differences in behavior were introduced,
either intentionally or by placing a lower priority
on implementing particular features.

\subsection{Bootloader integration}

On illumos, \texttt{beadm} is integrated with the bootloader.

This means that~\cite{illumos-grub-integration-code-analysis}:
\begin{itemize}
\item It updates the boot menu (that contains a selection of boot environments)
  when necessary.
\item When a boot environment is activated, it sets the menu's default to it.
\item It installs the bootloader to the pool's devices when necessary.
\item When listing boot environments, to determine if one is
  active on boot, it first checks the GRUB default
  (and, only if that fails, it checks the pool's \texttt{bootfs} property).
  % TODO Mention that our beadm checks only the latter
\end{itemize}

\subsubsection{Updating the boot menu}

To ensure that the boot menu contains an up-to-date selection of boot
environments, it has to be updated:
\begin{itemize}
\item on \texttt{beadm create} (when creating a boot environment),
\item on \texttt{beadm destroy},
\item on \texttt{beadm rename},
\item and, possibly, on \texttt{beadm activate} -- to change its default to
  the newly-active boot environment.
\end{itemize}

In illumos, when illumos GRUB1 is used, the bootloader menu is parsed
and modified by \texttt{libbe} directly. When
\texttt{loader}~\cite{illumos-loader} is used, modifying the menu is
unnecessary, as \texttt{loader} can list boot environments directly at
boot time.

In most of the GNU/Linux distributions that use GRUB2, the standard
approach is for the user not to write (or modify) the boot menu directly,
but to generate a menu from the scripts in \texttt{/etc/grub.d} using
\texttt{grub-mkconfig(8)}. To be consistent with this, a new
\texttt{/etc/grub.d} script that produces a selection of boot environments
is provided and the \texttt{beadm} port runs \texttt{grub-mkconfig(8)} to
regenerate the menu when necessary.
% TODO write that this is configurable with BE_REGENERATE_MENU_COMMAND

Note that it also does this on \texttt{beadm activate}. While it is
unnecessary for setting the default entry (for the reason described in the
next section), this was done as a convenience so that the user could
regenerate the menu just by activating a boot environment (instead of using
GRUB2 tools directly or indirectly causing the menu to be regenerated by
running \texttt{beadm create}, \texttt{destroy}, or \texttt{rename}).

\subsection{Boot environment activation}

To activate a boot environment, \texttt{beadm} should:

\begin{enumerate}
\item Update the root pool's bootfs property to reference
  the~given~boot environment.

  In the GNU/Linux port, this is done only for compatibility with other
  tools --- the proposed boot system does not use the property.
\item Regenerate the GRUB2 menu.
  This is done as a convenience so that the user can update the menu
  without modifying boot environments or using GRUB2 tools directly.
\item Make the given boot environment's GRUB2 menu entry the default.
\end{enumerate}

Unlike in Solaris-based operating systems, the \texttt{beadm~activate}
command does not rebuild the initramfs. In the authors' experience,
doing this on GNU/Linux is necessary only as part of a system upgrade:
when upgrading the kernel or its modules; so it is left for the system
administrator to do at that time.

In addition to this, \texttt{beadm activate} installs the bootloader
to the boot area of the pool's disks when both of the following
conditions apply:
\begin{itemize}
\item There are no partitions with unknown (non-Solaris)
  GUIDs (GPT partition types) on any of them.

  This is done as a heuristic to check if another operating system is
  installed on a disk.

\item They don't have a newer version of the bootloader installed
  already~\cite{illumos-install-bootloader}.
\end{itemize}

This is not implemented in the Linux port, as this would introduce
potentially destructive behavior (e.g.\ overwriting the MBR's boot
area when the user does not expect it), which would require more
extensive testing.
