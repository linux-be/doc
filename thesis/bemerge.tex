\chapter{bemerge} \label{chapter-bemerge} % bemerge
\markboth{}{bemerge}

\section{Introduction}

Currently, there is no package manager on any GNU/Linux distribution to have
support for boot environments, as they were not available before this work.
However, Solaris's package manager -- \texttt{pkg} -- has a few options for
interacting with them. It is possible to port at least part of it to GNU/Linux.
However, there are plenty of package managers for GNU/Linux, so the adoption
of a new one is improbable.

To have the boot environment options without porting \texttt{pkg},
a small compatibility layer is provided --
\texttt{bemerge}. The intent is to provide the user with a similar experience
to what Solaris administrators have without a need to heavily modify (or at all)
any package manager. Moreover, such a solution makes it portable and not limited
to Gentoo Linux (which has been used in this work).

\subsection{Examples}

The following examples use \texttt{emerge} -- the official package manager for
Gentoo Linux. Instead of appropriate options, \texttt{emerge-opts} is used as a
placeholder.

\begin{itemize}
\item Execute a package management operation in the running boot environment
    \begin{lstlisting}[language=bash]
    bemerge -- emerge emerge-opts
    \end{lstlisting}
\item Execute a package management operation in a boot environment with the given name
    \begin{lstlisting}[language=bash]
    bemerge -b existing-be -- emerge emerge-opts
    \end{lstlisting}
\item Execute a package management operation in a new boot environment
  with an automatically-generated name
  (created as a clone of \texttt{existing-be})
    \begin{lstlisting}[language=bash]
    bemerge -e existing-be -- emerge emerge-opts
    \end{lstlisting}
\item Execute a package management operation in a new boot environment with the given name
    \begin{lstlisting}[language=bash]
    bemerge -e existing-be -b new-be -- emerge emerge-opts
    \end{lstlisting}
\end{itemize}

In each invocation of \texttt{bemerge}, \texttt{-{}-} is used to take advantage
of the \texttt{POSIX}-compatible option parsing (\texttt{getopt(3p)}). The
option-scanning is forced to end when \texttt{-{}-} is encountered. This means
that \texttt{bemerge} can have switches (\texttt{-e} and \texttt{-b}) already
used by \texttt{emerge} without a conflict.

\section{Design decisions}

\subsection{Installation to a non-running boot environment}

\texttt{emerge} supports installation of packages to an alternate root with the
\texttt{-{}-root=ROOT} option. By default, build-time dependencies are installed to
both \texttt{/} and \texttt{ROOT} (the argument of \texttt{-{}-root}), while
run-time dependencies -- only to \texttt{ROOT}. The user can decide what should
be built and installed with the \texttt{-{}-root-deps} option. In order not to
install build-time dependencies of the package to \texttt{ROOT},
\texttt{-{}-root-deps=rdeps} should be used.  If \texttt{-{}-root-deps} is given
with no argument, it will not install build-time dependencies to \texttt{/},
which will result in a build failure. A detailed explanation is available in
\texttt{emerge(1)}.

% TODO: describe --root-deps experiments

The initial assumption was that \texttt{bemerge} should perform changes in
isolation from other boot environments present on the system.
\texttt{chroot} has been chosen as it guarantees that the running process is
limited to the given location. It is usually used to restrict access to the
system for users (\texttt{jail}\cite{bsd-jail}) and daemons.

\texttt{bemerge} is, in fact, a \texttt{chroot} wrapper. It is possible to have
a separate boot environment and restrict a program to it.

    \begin{lstlisting}[language=bash]
    bemerge -b some-be -- cmd
    \end{lstlisting}

Please note that, currently, bemerge mounts \texttt{/dev} inside the boot
environment, which means that the command being run has access to
all devices in \texttt{/dev} (excluding e.g. \texttt{/dev/pts} and
\texttt{/dev/shm} that are usually separate mountpoints).

\subsubsection{Special filesystems required by chroot}

\texttt{chroot} restricts the process to the given directory tree. As a
consequence, it loses access to the special filesystems. The special filesystems
used by a typical \texttt{GNU/Linux} distribution are: \texttt{procfs},
\texttt{sysfs}, \texttt{devtmpfs}, \texttt{tmpfs}, \texttt{debugfs}, and more.
They are used for communication with the \texttt{Linux} kernel.

The list of files used by a process can be retrieved by running it under
\texttt{strace} with proper arguments. \texttt{strace -e trace=file} will list
all system calls which take a file name as an argument, i.e. \texttt{open},
\texttt{stat}, \texttt{chmod}, \texttt{unlink}.

The \texttt{proc} filesystem is commonly mounted at \texttt{/proc}. It is an
interface to kernel data structures containing information about processes. For
example, \texttt{/proc/[pid]/cmdline} holds the complete command line of the
process, \texttt{/proc/[pid]/mountinfo} -- information about mount points in
the process's mount namespace~\cite{man-procfs}. Most programs, for instance,
\texttt{mount}, access it through a special reference \texttt{/proc/self}. Some
programs access information about all processes, most notably, \texttt{ps} and
\texttt{top}.

The \texttt{sys} filesystem is usually mounted at \texttt{/sys} and provides
access to information about devices, kernel modules, filesystems, and other
kernel components. For example, \texttt{/sys/bus} contains one subdirectory for
each of the bus types in the kernel, \texttt{/sys/block} -- contains one
symbolic link for each block device that has been discovered on the
system~\cite{man-sysfs}.  \texttt{/sys/bus/usb/devices} is used by programs like
\texttt{lsusb}, and \texttt{/sys/block} -- by \texttt{mount}.

\texttt{devtmpfs} is a type of tmpfs (a filesystem keeping all its contents in a
virtual memory) which is maintained by \texttt{udev} (a dynamic device
management utility) and the kernel~\cite{commit-devtmpfs}. It is usually
mounted on \texttt{/dev} and provides a convenient access to all devices
available on the system. For example, \texttt{mount -o loop} will use it to
access \texttt{/dev/loop-control} to find a free loop device (a block device
that maps its data blocks to the blocks of a regular file in a filesystem or to
another block device~\cite{man-loop}).

The following special filesystems were recognized to be frequently used by
userspace programs:
\begin{itemize}
\item \texttt{procfs}
\item \texttt{sysfs}
\item \texttt{devtmpfs}
\end{itemize}

\texttt{bemerge} automatically mounts the special filesystems in the proper
locations. No code in \texttt{bemerge} is necessary to unmount them,
as \texttt{libbe} unmounts everything under the mountpoint when unmounting
the boot environment.

\subsection{Use of libbe instead of wrapping beadm}

It may be tempting to try implementing \texttt{bemerge} as a shell script,
however \texttt{beadm} output may be difficult to parse.
A standard, POSIX-compatible shell could not be used as features,
arrays and lists, are needed. It would drastically impact the portability
of the solution. As a result, such a solution would be error-prone.

Moreover, more features are supported by \texttt{libbe} than are available in
\texttt{beadm} command line interface. For instance, with \texttt{libbe}, it is
possible to create a new boot environment without providing a name -- an
automatically generated one will be used.

Additionally, the choice of C aligns with the programming language used for the
rest of the system. It has a great support in the build automation tools
(\texttt{make}) and, as a result, in \texttt{ebuild}.

% TODO: describe issues found in libbe that should be reported to upstream;
% probably in a different (later) chapter

\subsection{Automatic snapshot creation during package installation}

\texttt{portage} does not have a direct support for \texttt{hooks} - commands
executed when a certain action occurs. \texttt{portage} uses an environment file
which is sourced for each package installation phase. \texttt{bemerge} provides
a file that can be used by \texttt{portage}.

When the \texttt{bemerge} package is installed, the user is informed that a
modification of the \texttt{/etc/portage/bashrc} file is required if snapshots
are to be created automatically. This feature is optional, as having a big
number of snapshots may impact performance of the \texttt{zfs} commands.

Currently, there are only two snapshots created for each package installation --
before files are copied to the proper location in the root file system
(\texttt{preinst}) and after that (\texttt{postinst}).

If \texttt{/etc/bemerge/portage-hook.sh} is executed, it checks the value of
\texttt{EBUILD\_PHASE} - an environment variable set by \texttt{portage},
and of \texttt{BE\_NAME} - an environment variable set by \texttt{bemerge}. The
name of the snapshot consists of the category of the package, its name and
version, and the \texttt{ebuild} phase. It is constructed by the expression
below.

    \begin{lstlisting}[language=bash]
    DESC="${CATEGORY}:${PF}:${EBUILD_PHASE}"
    \end{lstlisting} % $

A \texttt{:} separator has been chosen for the following reasons:
\begin{itemize}
\item The \texttt{Package Manager Specification}\cite{gentoo-pms} guarantees that it
is not used in the package name,
\item it is accepted by \texttt{zfs} as part of a valid snapshot name \cite{solaris-zfs-name-reqs},
\item it is easy to retrieve each value. For example, to list all packages (with
their versions) installed in \texttt{be-name}, run:
    \begin{lstlisting}[language=bash]
    beadm list -a be-name | awk -F'[@:]' '/@/ {print $3}' | uniq
    \end{lstlisting} % $
\end{itemize}

Please note that, in the current implementation of \texttt{bemerge}, the
\texttt{BE\_NAME} variable is not set when the command is executed in the
running boot environment, so no snapshots will be created in this case.

\input{thesis/bemerge_comparison}
