\chapter{Build automation} \label{chapter-build-automation} % Build automation
\markboth{}{Build automation}

\section{Introduction}

Build automation was a big part of the project and greatly increased
productivity and reliability. After each published commit there was a guarantee
that \texttt{ZFS} and \texttt{bemerge} built and worked correctly.

The ported boot environments require a unique setup:
\begin{itemize}
\item packages must be installed in the correct versions,
\item compatible ZFS kernel modules must be loaded,
\item \texttt{ROOT} dataset layout must be as specified,
\item the bootloader must be compatible with ZFS.
\end{itemize}

\noindent
Moreover, the tests have additional requirements:
\begin{itemize}
\item it should be possible to update the bootloader,
\item it should be possible to see/record the output of the bootloader,
\item the state of the environment should be reset before each build.
\end{itemize}

These unique needs mean that virtual machines must be used. The Kernel-based
Virtual Machine (KVM) has been chosen as it has the best performance and
support. \texttt{libvirt} was used as a convenient tool for management of
virtual machines with its command-line interface -- \texttt{virsh}. It is
possible to use snapshots of virtual machines in \texttt{libvirt}, but using
\texttt{zfs}'s interface directly (for manipulating the virtual disk devices'
snapshots) was more suitable for the project's needs.

\section{Virtual machine build}

Initially, the virtual machine was created manually. The first issue was to find
a live CD with \texttt{zfs} available. Conveniently, Gentoo Linux's LiveDVD
has it.

All the steps are noted so that it will be possible to reproduce the setup.

There was an attempt to automate the process with \texttt{packer}. It is mainly
used to create Amazon Machine Images or Vagrant boxes\cite{vagrant-boxes}. Two
open-source projects define a configuration needed to build a Gentoo Linux
image: \texttt{sorah/gentoo-build}\cite{packer-repo-1}, and
\texttt{d11wtq/gentoo-packer}\cite{packer-repo-2}. Neither of the above uses ZFS
as a root file system, so modifications were necessary.

Unfortunately, \texttt{packer} has terrible error handling. If any of the steps
fails, all the results are discarded. This behavior can be slightly changed by a
special flag -- \texttt{-on-error=ask}. Nevertheless, when the step is repeated
and succeeds, the \texttt{box} is not built. For these reasons, this approach
has been abandoned.

After the manual setup, it is possible to start the virtual machine from the
\texttt{virsh} interface (an example XML definition of the \texttt{libvirt}
virtual machine is available in the project's repository):
    \begin{lstlisting}[language=bash]
    virsh start GentooBE
    \end{lstlisting}

or directly, through \texttt{qemu}:
    \begin{lstlisting}[language=bash]
    qemu-system-x86_64 -enable-kvm \
        -cpu host \
        -drive file="/path/to/image",if=virtio \
        -netdev user,id=vmnic,hostname=Gentoo-VM \
        -device virtio-net,netdev=vmnic \
        -m 3G \
        -serial stdio \
        -display none
    \end{lstlisting}

\section{Build process}

Docker is an open platform for developing, shipping, and running
applications \cite{docker-intro}. It provides tools to build images and run
containers. Containers guarantee a predictable and reproducible environment
without the computational overhead of virtual machines.

In the \texttt{zfs} project, two images have been built. One of them was made
to mimic the virtual machine environment in order to build the \texttt{zfs}
package. The other was built automatically as part of the zfs build process
to include the compilation results (\texttt{zfs} installed). It was then used by
the bemerge project to build the \texttt{bemerge} package, as it required
\texttt{libbe} provided by the zfs with ported boot environments.

\section{Running the tests}

For convenience, wrapper scripts were provided -- Figure~\ref{fig-wrapper}.
The following one starts the
virtual machine named \texttt{GentooBE-ci}. It runs the commands given as
arguments, and guarantees a clean shut down afterwards. In the case when the
virtual machine is unresponsive, it forces it off (similar to a power cut).
\begin{figure}[h]
    \begin{lstlisting}[language=bash]
    #!/bin/sh
    name="GentooBE-ci"

    startVM() {
        virsh start $name
    }
    stopVM() {
        virsh shutdown $name
        local t=8
        # Wait for VM to shut down gracefully
        while [ "$t" -gt 0 ]; do
            if [ "$(virsh list --all | awk "/$name/"'{print $3}')" = "shut" ]
            then
                break
            fi
            sleep "$t"
            t="$((t-1))"
        done
        if [ "$t" -eq 0 ]; then
            virsh destroy $name
        fi
    }
    startVM
    eval $@
    exit_code="$?"
    stopVM

    exit $exit_code
    \end{lstlisting}
\caption{Contents of \texttt{wrapper.sh}}
\label{fig-wrapper}
\end{figure}

\clearpage
Serial console was the communication channel. It is enough to run \texttt{virsh
 console GentooBE-ci} to get an access to it. After that, the test program can
communicate with it. \texttt{Expect} \cite{expect} -- an extension to the Tcl
scripting language -- defines utilities to write to the console -- \texttt{send}
-- and to match its output -- \texttt{expect}. For convenience, a few functions
were defined, based on these primitives.

For example, to log in, first the boot sequence is matched, then the login
prompt, and then the username can be typed (with a carriage return).
\begin{figure}[h]
    \begin{lstlisting}[language=bash]
    proc exp_login { id } {
        exp_timeout $id {Linux version}
        exp_timeout $id {Importing ZFS pool ci}
        exp_timeout $id {login:}

        send -i $id "root\r"

        exp_prompt $id
    }
    \end{lstlisting}
\caption{Example implementation of \texttt{exp\_login} function}
\end{figure}

Similar functions were created to:
\begin{itemize}
\item wait for the given input for a limited time -- \texttt{exp\_timeout},
\item wait for the shell prompt to be displayed -- \texttt{exp\_prompt},
\item run a command and check for a 0 exit value -- \texttt{exp\_cmd},
\item export an environment variable defined in the local shell to the virtual
    machine's -- \texttt{exp\_export}.
\end{itemize}

As a result, installation of the package in the virtual machine is trivial -- Figure~\ref{fig-install}.
First, all the required environment variables are set in the virtual machine and
then the correct installation command is run.
\begin{figure}[h]
    \begin{lstlisting}[language=bash]
    exp_export $vm PKGDIR
    exp_export $vm zfs_LIVE_REPO
    exp_export $vm zfs_LIVE_COMMIT
    exp_export $vm zfs_kmod_LIVE_REPO
    exp_export $vm zfs_kmod_LIVE_COMMIT

    exp_cmd $vm "emerge -vtK --binpkg-respect-use=n '=sys-fs/zfs-9999'"
    \end{lstlisting}
    \caption{Example script, written in \texttt{Expect}, installing
      \texttt{sys-fs/zfs} on the virtual machine}
    \label{fig-install}
\end{figure}

% TODO: mention that the same paths may be used to correct setup of NFS mounts

\newpage
\section{Reproducible virtual machine state}

A ZFS volume (\texttt{zvol}) is a dataset that represents a block device
\cite{zfs-volume}. Due to the fact that a virtual machine's disk is such a
volume, it is possible to save its contents -- \texttt{zfs snapshot} -- and
reset it -- \texttt{zfs rollback}. Even if a built or a test resulted in a
corrupted system, the state is completely reset with the next run. Moreover, the
image can be cloned -- \texttt{zfs clone} -- and updated in a separate
instance. It can be easily converted to different formats -- \texttt{qemu-img
convert -f raw -O qcow2 /dev/zvol/pool/device output.qcow2}. It was
extremely useful in the debugging and review as the image could be distributed
with ease. Additionally, all these actions can be performed without any downtime
of the original instance.
