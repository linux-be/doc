\section{User documentation}

\subsection{System requirements}

System requirements have been described in section \ref{system-reqs}.

\subsection{User guide}

\subsubsection{Installation}

\paragraph{bemerge}\mbox{} \\

An ebuild\cite{ebuild} is available in the \gls{bemerge} repository.
In order to use it, provide a path to the source
to the build system. It may be done by an environment variable:
% FIXME once the repository is published or some version tagged a better way to distribute can be described here.
    \begin{lstlisting}[language=bash]
    export bemerge_LIVE_REPO="$(pwd)"
    \end{lstlisting}
Please note that the command above must be executed in the repository root path.
Otherwise, "\$(pwd)" should be replaced by a proper path.

To build and install the package to the system use the command below:
    \begin{lstlisting}[language=bash,linewidth=15cm]
    ebuild --skip-manifest sys-apps/bemerge/bemerge-9999.ebuild merge
    \end{lstlisting}

In order to generate additional code documentation a USE flag\cite{use-flag} must be set.
By default, only the \lstinline[columns=fixed]{bemerge(8)} manual page is installed.
It can be done temporarily by setting an environment variable:
    \begin{lstlisting}[language=bash]
    export USE="doc"
    \end{lstlisting}
or modifying a portage\cite{portage} configuration file:
    \begin{lstlisting}[language=bash]
    echo "sys-apps/bemerge doc" >> /etc/portage/package.use/bemerge
    \end{lstlisting}
After setting the USE flag, rebuild the package:
    \begin{lstlisting}[language=bash,linewidth=15cm]
    ebuild --skip-manifest sys-apps/bemerge/bemerge-9999.ebuild merge
    \end{lstlisting}

\paragraph{beadm}\mbox{} \\

The official live ebuilds from portage\cite{portage} can be used to install
our forks of \texttt{zfs} with \gls{beadm} included.

Specify that \texttt{zfs} and \texttt{zfs-kmod} should be installed
from the local clone of the forked repository:
% FIXME A 4-space indent has to be used below for some reason.
\begin{lstlisting}[language=bash]
    export zfs_LIVE_REPO="/path/to/zfs/repository"
    export zfs_LIVE_BRANCH="beadm"
    export zfs_kmod_LIVE_REPO="/path/to/zfs/repository"
    export zfs_kmod_LIVE_BRANCH="beadm"
\end{lstlisting}

Install \texttt{zfs}:
\begin{lstlisting}[language=bash]
    emerge -vt '=sys-fs/zfs-9999'
\end{lstlisting}

Rebuild the initramfs to use the compatible (\texttt{9999}) version
of the ZFS kernel modules on boot:
\begin{lstlisting}[language=bash]
    genkernel initramfs --virtio --no-clean --no-mountboot --zfs \
      --bootloader=grub2 --makeopts=-j5 \
      --callback="emerge @module-rebuild"
\end{lstlisting}

\paragraph{grub}\mbox{} \\

\begin{enumerate}
\item Make sure your GRUB2\cite{grub} installation directory
  is in a \gls{shared dataset} (not inside a \gls{be}). If not, make it so.

  If you have a different GRUB2 installation directory
  (e.g.\ \texttt{/boot/grub2} instead of \texttt{/boot/grub})
  (e.g.\ if you are going to install \texttt{sys-boot/grub}
  with \texttt{USE=multislot}),
  use \texttt{grub2} instead of \texttt{grub} below.

  Replace \texttt{\$pool} below with your root pool name
  and \texttt{\$disk} with the device you normally install GRUB2 onto.

  \begin{lstlisting}[language=bash]
    mv /boot/grub{,.b}
    zfs create -o mountpoint=/boot/grub $pool/grub
    rsync -av /boot/grub.b/. /boot/grub/
    rm -r /boot/grub.b
    grub-install $disk
  \end{lstlisting}

\item Like \gls{beadm}, install \texttt{grub} using the official ebuild
  with a custom repository:

  \begin{lstlisting}[language=bash]
    export grub_LIVE_REPO="/path/to/grub/repository"
    export grub_LIVE_BRANCH="beadm"
    emerge -vt '=grub-9999'
  \end{lstlisting}

\item Tweak \texttt{/etc/grub.d/11\_linux\_be} to have the proper pool name
  and kernel version.

\item If the GRUB2 tools/directories
  are called \texttt{grub2} instead of \texttt{grub}
  (e.g. \texttt{grub2-mkconfig} instead of \texttt{grub-mkconfig})
  (e.g. if you installed \texttt{sys-boot/grub}
  with \texttt{USE=multislot}), adjust \texttt{/etc/default/be} to match:

  \begin{lstlisting}[language=bash,linewidth=15.5cm]
    BE_REGENERATE_MENU_COMMAND=grub2-mkconfig -o /boot/grub2/grub.cfg
    BE_SET_DEFAULT_MENU_ENTRY_COMMAND=grub2-set-default
  \end{lstlisting}

\item Set \texttt{GRUB\_DEFAULT=saved} in \texttt{/etc/default/grub}.
\end{enumerate}

\paragraph{Finishing the installation}\mbox{} \\

\begin{enumerate}
\item Activate the \gls{running be} with \gls{beadm}
  to regenerate the boot menu to have \glspl{be}
  and to make the \gls{running be} the default on the next boot:

  \begin{lstlisting}[language=bash]
    beadm activate -v <running BE name>
  \end{lstlisting}

  This is needed only once --- when \gls{beadm} is used to manipulate
  \glspl{be}, the boot menu is automatically regenerated if necessary.

\item Reboot.
\end{enumerate}

We plan to make the installation more straightforward once the code is opened to the public.

\subsubsection{Configuration}

\paragraph{bemerge}\mbox{} \\
In order for \gls{bemerge} to create snapshots before and after package installation,
portage\cite{portage} must be configured to call the hooks provided by \gls{bemerge} package.
It is an optional feature of \gls{bemerge} so it is not done automatically
during package installation. Edit `/etc/portage/bashrc' file:
    \begin{lstlisting}[language=bash]
    echo "source /etc/bemerge/*" >> /etc/portage/bashrc
    \end{lstlisting}

\subsubsection{Usage}

\paragraph{beadm}
\begin{itemize}
\item Create a new boot environment based on the running boot environment
    \begin{lstlisting}[language=bash]
    beadm create new-be
    \end{lstlisting}
\item Create a new boot environment based on a specified boot environment
    \begin{lstlisting}[language=bash]
    beadm create -e existing-be new-be
    \end{lstlisting}
\item Create a new boot environment based on an existing snapshot
    \begin{lstlisting}[language=bash]
    beadm create -e existing-be@snapshot-name new-be
    \end{lstlisting}
\item Create a snapshot of an existing boot environment
    \begin{lstlisting}[language=bash]
    beadm create existing-be@snapshot-name
    \end{lstlisting}
\item Destroy a boot environment
    \begin{lstlisting}[language=bash]
    beadm destroy existing-be
    \end{lstlisting}
\item Destroy a snapshot of a boot environment
    \begin{lstlisting}[language=bash]
    beadm destroy existing-be@snapshot-name
    \end{lstlisting}
\item Display information about boot environment snapshots and datasets
    \begin{lstlisting}[language=bash]
    beadm list -a
    \end{lstlisting}
\item Mount a boot environment
    \begin{lstlisting}[language=bash]
    beadm mount existing-be
    \end{lstlisting}
\item Unmount a boot environment
    \begin{lstlisting}[language=bash]
    beadm umount existing-be
    \end{lstlisting}
\item Activate an existing, inactive boot environment
    \begin{lstlisting}[language=bash]
    beadm activate non-active-be
    \end{lstlisting}
\item Rename an existing, inactive boot environment
    \begin{lstlisting}[language=bash]
    beadm rename old-be new-be
    \end{lstlisting}
\end{itemize}

\paragraph{bemerge}
\begin{itemize}
\item Execute a \gls{pkg op} in the running boot environment
    \begin{lstlisting}[language=bash]
    bemerge -- emerge emerge-opts
    \end{lstlisting}
\item Execute a \gls{pkg op} in a boot environment with the given name
    \begin{lstlisting}[language=bash]
    bemerge -b existing-be -- emerge emerge-opts
    \end{lstlisting}
\item Execute a \gls{pkg op} in a new boot environment with an automatically-generated name
    \begin{lstlisting}[language=bash]
    bemerge -e existing-be -- emerge emerge-opts
    \end{lstlisting}
\item Execute a \gls{pkg op} in a new boot environment with the given name
    \begin{lstlisting}[language=bash]
    bemerge -e existing-be -b new-be -- emerge emerge-opts
    \end{lstlisting}
\end{itemize}

\subsection{Acceptance tests}

\subsubsection{Scenario}

\paragraph{bemerge}\mbox{} \\

\noindent
\begin{tabular}{l | l}
    Title & Description\\
    \hline
    Install                         & Installation steps described              \\
                                    & in the user documentation should work     \\
    \hline
    Execute a command               & Command must be of a valid format.        \\
    in the \gls{running be}         & \texttt{emerge -{}-version} may be used.    \\
    \hline
    Execute a command               & (as above)                                \\
    in a new (named) \gls{be}       &                                           \\
    \hline
    Execute a command               & (as above)                                \\
    in a new (auto-named) \gls{be}  &                                           \\
    \hline
    Execute a different command     & \texttt{ls} may be used.                  \\
                                    & It should fail to do that.                \\
    \hline
    Pass a signal to the command    & A maskable signal should be used.         \\
                                    & When the command exits it should also     \\
                                    & unmount the \gls{be}.                     \\
    \hline
    Create a snapshot before        & Configuration of the system must be changed.  \\
    and after package installation  & Snapshot names should contain package name,   \\
                                    & version and installation step.                \\
    \hline
    Manual page\cite{man-page}     & Output of \texttt{man 8 bemerge} should be inspected.\\
    \hline
    Source code documentation       & Output of \texttt{man 3 bemerge-be.c},           \\
    in the form of manual pages     & \texttt{man 3 bemerge-bemerge.c},                \\
    \cite{man-page}                 & \texttt{man 3 bemerge-signals.c} should be inspected.\\
\end{tabular}
\\
\\
Moreover, \gls{bemerge} should not modify contents of other \glspl{be}.

\subsubsection{Results}

\paragraph{bemerge}\mbox{} \\

Most of tests are executed as a part of Continuous Integration.
It consists of two stages: `compile' and `vm-tests'. The former
builds the \gls{bemerge} package in a docker image which was
constructed from the `beadm' branch of the `zfs' repository. The latter stage
installs compiled package in a virtual machine and executes tests
defined in `tests/acceptance.sh' file. This file uses the BATS\cite{bats}
framework which is required to execute it.

\noindent
\makebox[\textwidth]{
\begin{tabular}{p{4cm} | p{4cm} | p{8cm}}
    Title & Command & Result\\
    \hline
    Install
        & \texttt{ebuild -{}-skip-manifest sys-apps/bemerge/bemerge-9999.ebuild merge}
        & The command was executed successfully. User has been asked to update the configuration of portage\cite{portage}.\\
    \hline
    Execute a command in the \gls{running be}
        & \texttt{bemerge -{}- emerge -{}-version}
        & The command was executed successfully. Information about execution in the running boot environment
            has been printed. The emerge command's version has been printed.\\
    \hline
    Execute a command in a new (named) \gls{be}
        & \texttt{bemerge -e gentoo -b gentoo-new -{}- emerge -{}-version}
        & The command was executed successfully. Information about the creation of a new boot environment,
            mounting and unmounting has been printed. The emerge command's version has been printed.
            A new \gls{be} with \texttt{gentoo-new} name is available to the system.\\
    \hline
    Execute a command in a new (auto-named) \gls{be}
        & \texttt{bemerge -e gentoo -{}- emerge -{}-version}
        & The command was executed successfully. Information about the creation of a new boot environment,
            mounting and unmounting has been printed. The name of the new \gls{be} has been printed.
            The emerge command's version has been printed.
            A new \gls{be} with \texttt{gentoo-1} name is available to the system.\\
    \hline
    Execute a different command
        & \texttt{bemerge -{}- ls}
        & The command was executed unsuccessfully.
            Information that \texttt{ls} is not supported has been printed.\\
        & \texttt{bemerge -b gentoo-new -{}- ls}
        & The command was executed unsuccessfully.
            Information that \texttt{ls} is not supported has been printed.\\
    \hline
    Pass a signal to the command
        & \texttt{bemerge -b gentoo-new -{}- emerge -av app-text/tree \&; kill -s SIGINT \$!}
        & The command was executed unsuccessfully.
            Information about mounting of a \texttt{gentoo-new}, interrupting the \texttt{emerge} command,
            and unmounting of a \texttt{gentoo-new} has been printed.
            \texttt{gentoo-new} is unmountinged.\\
    \hline
    Create a snapshot before and after package installation
        & \texttt{bemerge -b gentoo-new -{}- emerge -vt app-text/tree}
        & The command was executed successfully. Information about mounting and unmounting has been printed.
            Inspection of \texttt{beadm list -a} confirms that two new snapshots has been created for
            the \texttt{gentoo-new} \gls{be}.\\
    \hline
    Manual page\cite{man-page}
        & \texttt{man 8 bemerge}
        & The output contains descriptions of different options and arguments.\\
    \hline
    Source code documentation in the form of manual pages\cite{man-page}
        & \texttt{man 3 bemerge-be.c}
        & The output contains documentation of each function defined in \texttt{cmd/bemerge/be.c} file.\\
        & \texttt{man 3 bemerge-bemerge.c}
        & The output contains documentation of each function and one structure defined
            in \texttt{cmd/bemerge/bemerge.c} file.\\
        & \texttt{man 3 bemerge-signals.c}
        & The output contains documentation of each function defined in \texttt{cmd/bemerge/signals.c} file.\\
\end{tabular}
} % end of \makebox

\newpage
\paragraph{beadm}\mbox{} \\

\subparagraph{\texttt{beadm list}}

Behaving as expected:
\begin{itemize}
\item running without options
\item listing datasets and snapshots with \texttt{-a}
\item listing datasets with \texttt{-d}
\item listing snapshots with \texttt{-s}
\item running with \texttt{-d} and \texttt{-s} at the same time - refused
\item sorting with \texttt{-k date|name|space}
\end{itemize}

\subparagraph{\texttt{beadm mount/umount/unmount}}

Behaving as expected:
\begin{itemize}
\item not passing a mountpoint - one with a random name is created automatically
\item using a nonexistent mountpoint - one is created automatically
\item trying to unmount the running BE - refused
\item trying to mount/unmount twice - refused
\item trying to unmount a nonexistent directory - refused
\item trying to unmount a directory nothing is mounted on - refused
\item passing a mounted BE to unmount
\item passing a mountpoint of a mounted BE to unmount
\item mounting shared filesystems using \texttt{-s rw}
  \begin{itemize}
  \item including children with inherited mountpoints
  \item including children of ones with \texttt{canmount=off}
  \end{itemize}
\end{itemize}

Not behaving as expected:
\begin{itemize}
\item mounting a BE on a mountpoint where another BE is already mounted succeeds
  and requires the user to unmount the mountpoint manually
  before unmounting both BEs is possible.
\item mounting shared filesystems using \texttt{-s ro} mounts them read-write instead.
\item \texttt{beadm unmount -f be} does not seem to make a difference if
  the mounted BE is being used by a running program.
\end{itemize}

\subparagraph{\texttt{beadm create}}

Behaving as expected:
\begin{itemize}
\item \texttt{beadm create gentoo-2} - clones from the running BE
\item \texttt{beadm create -e non-activeBeName gentoo-3}
\item \texttt{beadm create -e activeBeName gentoo-3}
\item \texttt{-e beName@snapshot} (manual + \texttt{beadm}-created)
\item \texttt{beadm create -p pool gentoo-4}
\item running in a chroot:
  \begin{itemize}
  \item \texttt{beadm list} recognizes the BE \texttt{chroot}ed into
    as the running BE.
  \item \texttt{beadm create} creates a BE
    from the BE \texttt{chroot}ed into.
  \end{itemize}
\item running in a bind-mounted chroot
\item \texttt{-a} to activate the BE
\end{itemize}

Not behaving as expected:
\begin{itemize}
\item \texttt{-o property=value} has no effect
\item \texttt{-d description} has no effect -
  the BEs menu title is the default, not \texttt{description}
\end{itemize}

\subparagraph{\texttt{beadm activate}}

Behaving as expected. Promotes the newly-active BE.

\subparagraph{\texttt{beadm destroy}}

Behaving as expected:
\begin{itemize}
\item for a clone with no dependent clones
\item trying to remove the running BE - refused
\item for a clone with dependent clones - requires the \texttt{-s} option, and, if it's passed, changes the dependent clones' origins
\item \texttt{-F} to destroy without user confirmation
\end{itemize}

\subparagraph{\texttt{beadm rollback}}

Behaving as expected
both as \texttt{beadm rollback be@snapshot}
and \texttt{beadm rollback be snapshot}.

\subparagraph{\texttt{beadm rename}}

Behaving as expected with a BE name.
Refuses to rename a snapshot (\texttt{be-name@snapshot}).

\subparagraph{Alternate BE location (instead of \texttt{pool/ROOT})}

If \texttt{BENAME\_STARTS\_WITH} is specified in \texttt{/etc/default/be},
BEs should be under the pool's root dataset and their names
should start with the value of \texttt{BENAME\_STARTS\_WITH}.

If it is not specified or is an empty string, \texttt{beadm}
behaves normally - BEs are under \texttt{pool\_name/ROOT}
and can have any valid dataset names, but also should not contain spaces
and \texttt{\%} characters.

If it is specified properly, \texttt{beadm list} behaves as expected.
