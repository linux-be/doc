#!/bin/bash

repo=registry.gitlab.com/linux-be/doc

cd "$(dirname "$0")"

docker build -t "$repo" . && \
    docker push "$repo"
