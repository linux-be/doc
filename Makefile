TEX_OPTS = -interaction=scrollmode -halt-on-error
TEX = pdflatex $(TEX_OPTS)
XTEX = xelatex $(TEX_OPTS)
MPOST = mpost -interaction=nonstopmode
INKSCAPE = inkscape -z -d 600 -C
PLANTUML = DISPLAY="" java -jar vendor/plantuml.jar -tsvg

.PHONY: all doc clean
all: doc thesis presentation # validate_examples

TEX_IN := $(wildcard tex/*.tex)
TEX_OUT := $(TEX_IN:.tex=.aux)
THESIS_IN := $(wildcard thesis/*.tex)
THESIS_OUT := $(THESIS_IN:.tex=.aux)
MP_IN := $(wildcard mp/*.mp)
MP_DEPS := $(wildcard mp/**/*.mp)
MP_OUT := $(MP_IN:.mp=.mps)
SVG_IN := $(wildcard svg/*.svg)
SVG_OUT := $(SVG_IN:.svg=.pdf_tex)
PU_IN := $(wildcard pu/*.pu)
PU_OUT := $(PU_IN:.pu=.pdf_tex)
TEMP := $(wildcard *.1 *.aux *.log)

DOC_OBJS := Documentation.pdf
THESIS_OBJS := titlepage.pdf thesis.pdf

thesis: $(THESIS_OBJS)
doc: $(DOC_OBJS)
validate_examples:
	cd json && make

presentation: Presentation.pdf

.SECONDARY: $(MP_OUT) $(SVG_OUT) $(PU_OUT)

titlepage.pdf: titlepage.tex
	$(XTEX) titlepage.tex

thesis.pdf: thesis.tex $(THESIS_IN)
	$(XTEX) thesis.tex
	$(XTEX) thesis.tex

Documentation.pdf: Documentation.tex $(TEX_IN) $(MP_OUT) $(SVG_OUT) $(PU_OUT)
	$(TEX) Documentation.tex
	makeglossaries Documentation.glo
	$(TEX) Documentation.tex

Presentation.pdf: Presentation.tex
	$(TEX) Presentation.tex
	$(TEX) Presentation.tex

%.mps: %.mp $(MP_DEPS)
	$(MPOST) $<
	mv $(notdir $*).1 $*.mps

%.pdf_tex: %.svg
	$(INKSCAPE) --file=$< --export-pdf=$(@:_tex=) --export-latex

%.svg: %.pu
	$(PLANTUML) $<

clean:
	rm -f *.aux *.lof *.lot *.toc $(THESIS_OBJS) $(THESIS_OUT) $(DOC_OBJS) $(TEX_OUT) $(MP_OUT) $(SVG_OUT) $(PU_OUT) $(TEMP)
