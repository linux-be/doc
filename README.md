# GNU/Linux Boot Environments (linux-be)

This project aims to provide an integrated Boot Environment setup for GNU/Linux.

## Documentation

The main place for up-to-date user and developer documentation
is the linux-be/zfs wiki.
[Installing linux-be](https://gitlab.com/linux-be/zfs/wikis/linux-be-installation/Installing-linux-be)
is a good starting point.

This project was done as a Bachelor thesis.
[The defended version of that thesis](https://gitlab.com/linux-be/doc/-/jobs/101021032/artifacts/file/thesis.pdf)
and [a document created to satisfy course requirements](https://gitlab.com/linux-be/doc/-/jobs/101021032/artifacts/file/Documentation.pdf)
have an older, but more complete, description of the project.
[The defense presentation](https://gitlab.com/linux-be/doc/-/jobs/101026914/artifacts/file/Presentation.pdf)
and the poster
([screen version](https://gitlab.com/linux-be/doc/-/jobs/101026914/artifacts/file/poster_screen.pdf),
[print version](https://gitlab.com/linux-be/doc/-/jobs/101026914/artifacts/file/poster_print.pdf))
are also available.
